function [float, model, perf] = floatGeneratePrediction(float, model)
% function for generating the mean and variance for the predictions
% input:
% float -- contain the information from the last iteration of Maximization
% model -- contain all the model parameters
% output:
% float -- contain now information about the mean, variance of each
%             predictions: float(i).Xpred & float(i).XMse
% model -- contain nor information about the model.MSE of the full trianset
% perf -- model performance struct, SST, SSE, MSE of the pooled data
% written by Yan Yan (yy2250@columbia.edu)

% order of the variables in the distributed 
%  1. lat
%  2. lon
%  3. ve
%  4. vn
%  5. spd
%  6. dist
%  7. cdm
%  8. kd490
%  9. t865
% 10. par
% 11. sst4
% 12. chlor_a


d = model.d


A = model.A;
C = model.C;
G = model.G;
S = model.S;
N = size(float,2);   
MSEtop =0;
MSEbottom = 0; 


NXPooled = 0;   
for i =1:N
    Tn = float(i).Tn;  
    NXPooled = NXPooled + Tn;
    float(i).Xpred = zeros(d,Tn);
    float(i).Xpred  = C*A*float(i).nu;  
    D = abs(float(i).Xpred - float(i).X).^2;
    float(i).MSE = sum(D(:))/ numel(float(i).Xpred); 
    
    MSEtop  = MSEtop + sum(D(:));  
    MSEbottom = MSEbottom + numel(float(i).Xpred); 
    
    float(i).Xvar = zeros(d,d,Tn);      
    for j = 1:Tn
        P = A*float(i).U(:,:,j)*A' +G;  
        PC = P*C'; 
        R = C*PC + S;          
        float(i).Xvar(:,:,j) = R; 
    end
   
end

perf.NXPooled = NXPooled;

% model.trainMSE = MSEtop /MSEbottom;  

%%% new block to generate the pooled 
%%% XPooled, XPredPooled,
%%% XSsePooled=(XPooled- XPredPooled)^2, XMsePooled
%%% XSsetotalPooled, XR2Pooled
% initialize
perf.XPooled = zeros(d,perf.NXPooled);
perf.XpredPooled = zeros(d,perf.NXPooled);

% load data
mark = 1;
for i =1:N
    Tn = float(i).Tn;  % length of the time series
    perf.XPooled(:, mark:mark+Tn -1) =  float(i).X;
    perf.XpredPooled(:, mark:mark+Tn -1) =  float(i).Xpred;
    mark = mark+Tn;
end

% compute metric for the pooled data
XMeanPooled = mean(perf.XPooled,2  );
DTotal = abs(perf.XPooled - repmat(XMeanPooled, 1, perf.NXPooled) ).^2;
perf.SSTSeperate = sum(DTotal,2);  % 12 SSTotal for all 12 features
perf.SSTPooled = sum(DTotal(:));

DResidual = abs(perf.XpredPooled - perf.XPooled).^2;
perf.SSESeperate = sum(DResidual, 2);  % 12 SSTotal for all 12 features
perf.SSEPooled = sum(DResidual(:));

perf.MSESeperate = perf.SSESeperate./ perf.NXPooled;
perf.MSEPooled = perf.SSEPooled/ numel(perf.XpredPooled);

perf.RMSESeperate = sqrt(perf.MSESeperate);
perf.RMSEPooled = sqrt(perf.MSEPooled);

perf.RsquaredSeperate= 1 - perf.SSESeperate ./ perf.SSTSeperate;
perf.RsquaredPooled = 1 - perf.SSEPooled / perf.SSTPooled;


end