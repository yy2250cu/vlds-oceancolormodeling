function [float, model, llh] = floatLDSFitting(float, model)
% function for LDS fitting multiple multivariate time series
%  input:
%  float  -- data structure for storing all the observations
%  model -- contain common LDS model parameters
%  output:
%  float  -- data structure for storing intermediate LDS results
%  model -- contains updated LDS model parameters
%  llh  -- trajectory of the log-likelihood functions
% written by Yan Yan (yy2250@columbia.edu)

N = model.N; 
tol = 1e-4; 
maxIter = 100;
for i = 1:N
float(i).llh = zeros(1, maxIter); % to confirm llh are all increasing
end
llh = zeros(1, maxIter);

for iter =2:maxIter
  % E step  
  fprintf(' *** Iteration %d completed *** \n', iter);
  for i = 1:N
     [float(i).nu, float(i).U, float(i).Ezz, float(i).Ezy, float(i).llh(iter)] = kalmanSmoother(float(i).X, model);
     llh(iter) = llh(iter) + float(i).llh(iter) ;
  end
  
  if (iter>2 && (llh(iter)-llh(iter-1) < tol*abs(llh(iter-1)))  ); break; end   % check for convergence
  %     M-step 
  model = floatMaximization(float, model);
end
llh = llh(2:iter);
