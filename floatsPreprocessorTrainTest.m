function [float_train, float_test]= ...
    floatsPreprocessorTrainTest(float_table, ...
    id_set_train, id_set_test)
% function for generating training and testing dataset
% input:
% float_table -- data for all float 
% id_set_train -- training set id
%  id_set_test --  testing set id
% output:
%  float_train -- training set data
% float_test -- testing set data
% written by Yan Yan (yy2250@columbia.edu)

id_count_train = length(id_set_train);
for i = 1:  id_count_train
    fprintf('training dataset %d \n', i);
    float_train(i).id = id_set_train(i);
    [float_train(i).X, float_train(i).Tn] = floatsPreprocessor(float_table, id_set_train(i));
end


id_count_test = length(id_set_test);
for i = 1:  id_count_test
    fprintf('testing dataset %d \n', i);
    float_test(i).id = id_set_test(i);
    [float_test(i).X, float_test(i).Tn] = floatsPreprocessor(float_table, id_set_test(i));
end


end