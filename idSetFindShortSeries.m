function id_too_short = idSetFindShortSeries(float_table, id_set, threshold)
% function to look for ids of very short float time series
% we would to remove those ids before cv
% input:
% float_table -- original data contains info on all floats
% id_set -- set contains all float ids, based on which we use a loop 
% threshold -- a cut off point where series are considered too short
% output:
% id_too_short -- the ids of those very short float time series
% written by Yan Yan (yy2250@columbia.edu)

N = length(id_set);
count = 1;
for i = 1: 1: N
    mask  = float_table.id == id_set(i);
    tmp = float_table(mask,:);
    len_tmp = size(tmp,1);
    if len_tmp <= threshold;
        id_too_short(count) = id_set(i);
        count = count + 1; 
    end
end


end




