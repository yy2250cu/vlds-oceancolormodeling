function plotFloatPredictionCv(float, model,figNo)
% function for visual the prediction and the original obervations
% input:
% float --  contains all the float information
% model -- contains model parameters
% generate the prediction plots for a few floats and a few variable
% written by Yan Yan (yy2250@columbia.edu)

id1 = 27139.03;
id2 = 43744.06; 
id3 = 34721.03;
id4 = 43746.06;
id5= 53363.03;

N = size(float, 2);

for i = 1:N   
    if float(i).id == id1   
        index(1) = i; end
    if float(i).id == id2  
        index(2) = i; end
    if float(i).id == id3	
        index(3) = i; end
    if float(i).id == id4	
        index(4) = i; end
    if float(i).id == id5	
        index(5) = i; end
end

figure(figNo);

for i=1:5      % 5 Floats
    indx = index(i);
    Tn = float(indx).Tn;
    % 3 Variables, chlor_a (12), lon(2), sst4(11), cdm(7)
    no1 = (i-1)*10 + 1;
    subplot(5,10,no1);
    plot(1:Tn,float(indx).X(12,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(12,:), '-c.');  % predicted
    xlabel('time', 'fontsize',14);
    ylabel('chlor\_a', 'fontsize',14);
    title(['float id ' num2str(float(indx).id ) ], 'fontsize',14);
    if no1 ==1 
        legendobj = legend('observations of cv data','predictions of cv data','location','northoutside'); % manually move it
        legendobj.FontSize = 14;
    end    
    %a=get(legendobj,'position')
    a=[0.0171*20,    0.9540,    0.1363*0.5,    0.0420*0.5];
    set(legendobj,'position',a);
    axis tight;
    hold off;
    
    no2 = (i-1)*10 + 2;
    subplot(5,10,no2);
    plot(1:Tn,float(indx).X(2,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(2,:), '-c.');  % predicted
    xlabel('time', 'fontsize',14);
    ylabel('lon', 'fontsize',14)

    axis tight;
    hold off;
    
    
    no3 = (i-1)*10 + 3;
    subplot(5,10,no3);
    plot(1:Tn,float(indx).X(11,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(11,:), '-c.');  % predicted
    xlabel('time', 'fontsize',14);
    ylabel('sst4', 'fontsize',14)
    axis tight;
    hold off;
    
    no4 = (i-1)*10 + 4;
    subplot(5,10,no4);
    plot(1:Tn,float(indx).X(7,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(7,:), '-c.');  % predicted
    xlabel('time', 'fontsize',14);
    ylabel('cdm', 'fontsize',14)
    axis tight;
    hold off;
    
    no5 = (i-1)*10 + 5;
    subplot(5,10,no5);
    plot(1:Tn,float(indx).X(8,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(8,:), '-c.');  % predicted
    xlabel('time', 'fontsize',14);
    ylabel('kd490', 'fontsize',14)
    axis tight;
    hold off;
    
    no6 = (i-1)*10 + 6;
    subplot(5,10,no6);
    plot(1:Tn,float(indx).X(9,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(9,:), '-c.');  % predicted
    xlabel('time', 'fontsize',14);
    ylabel('t865', 'fontsize',14)
    axis tight;
    hold off;
    
    no7 = (i-1)*10 + 7;
    subplot(5,10,no7); 
    plot(1:Tn,float(indx).X(6,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(6,:), '-c.');  % predicted

    xlabel('time', 'fontsize',14);
    ylabel('dist', 'fontsize',14)
    axis tight;
    hold off;
    
    no8 = (i-1)*10 + 8;
    subplot(5,10,no8); 
    plot(1:Tn,float(indx).X(10,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(10,:), '-c.');  % predicted

    xlabel('time', 'fontsize',14);
    ylabel('par', 'fontsize',14)
    axis tight;
    hold off;
    
    no9 = (i-1)*10 + 9;
    subplot(5,10,no9); 
    plot(1:Tn,float(indx).X(3,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(3,:), '-c.');  % predicted

    xlabel('time', 'fontsize',14);
    ylabel('ve', 'fontsize',14)
    axis tight;
    hold off;
    
    no10 = (i-1)*10 + 10;
    subplot(5,10,no10); 
    plot(1:Tn,float(indx).X(4,:), '-k.');   % observed
    hold on;
    plot(1:Tn,float(indx).Xpred(4,:), '-c.');  % predicted

    xlabel('time', 'fontsize',14);
    ylabel('vn', 'fontsize',14)
    axis tight;
    hold off;
    
end


end