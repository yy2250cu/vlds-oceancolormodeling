function id_set_remain = idSetRemove(id_set, id_set_remove)
% remove the elements in holdout set from id_set
% input:
% id_set -- the full ids
% id_set_remove -- the ids to be holdout and to be removed
% output:
% id_set_remain -- the remaining set after removing the holdous
% written by yan yan (yy2250@columbia.edu)

 M = length(id_set);
 N = length(id_set_remove);
 indx = zeros(N, 1);
 
 for i=1:N
       for j = 1:M
           if id_set_remove(i) == id_set(j)  
               indx(i) = j;
           end
       end
 end

id_set(indx) = [];
id_set_remain = id_set;

end