% demos for vLDS 
% using the cross-validated parameters for predictions
% written by Yan Yan (yy2250@columbia.edu)
clear all; close all; clc
pwd
float_table = readtable('DataPreProcessed/floatsDF_list_final_LDS_2d_4thrun.csv');
id_set = unique(float_table.id);  

id_set_holdout = [53367.03, 43598.03, 64113560.03, ...
			              11089.03, 63076.03];    
% identify series that are shorter than a threshold
threshold = 5;
id_too_short = idSetFindShortSeries(float_table, id_set, threshold);
id_set_short_removed= idSetRemove(id_set, id_too_short);  
id_set_cv= idSetRemove(id_set_short_removed, id_set_holdout); 

% take k=11 based on the "demo_2D_allFloatCV" results 
% use the script 'demo_2D_allFloatCV' to look into the
% cross-validation details and identify the best latent dimension k
k = 11; 

% fit on all the full CV set again
% preprocessing
[float_cv, float_holdout]= floatsPreprocessorCvHoldout(float_table,...
    id_set_cv, id_set_holdout);
N = size(id_set_cv,1);     
size_col= 12;    
d = size_col;     
fprintf('******** use cross-validated k = %d on the full cv set ******** \n', k);
model = onefloat_setModel(d,k,N);  

% model fitting using LDS on training dataset
[float_cv, model, llh_cv] = floatLDSFitting(float_cv, model);

% convergence
plotFloatLlhCv(float_cv, llh_cv, 1) 

%%%%%%%%%%%%%%%%%%%%%%%%%%
% predictions & visual on the cv dataset
[float_cv, model, perf_cv] = floatGeneratePrediction(float_cv, model);
% visualize the predictions of a few floats in the cv dataset
plotFloatPredictionCv(float_cv, model,2)  

%%%%%%%%%%%%%%%%%%%%%%%%%%
% predictions & visual on the holdout dataset
[float_holdout, llh_holdout] = floatLDSTestingllh(float_holdout, model);
[float_holdout, model, perf_holdout] = floatGeneratePrediction(float_holdout, model);
% visualize the predictions of a few floats in the test dataset
plotFloatPredictionHoldout(float_holdout, model,3);







