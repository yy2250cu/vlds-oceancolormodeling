% demos for generating the cross-validation data using vLDS 
% the float data contains {id, time, lat, lon, chlor_a}
% writen by Yan Yan (yy2250@columbia.edu)
clear all; close all; clc
pwd
float_table = readtable('DataPreProcessed/floatsDF_list_final_LDS_2d_4thrun.csv');
id_set = unique(float_table.id);  

%%% in this area for generating training and testing dataset
id_set_holdout = [53367.03, 43598.03, 64113560.03, ...  
			              11089.03, 63076.03];    
% identify series that are shorter than a threshold
threshold = 5;
id_too_short = idSetFindShortSeries(float_table, id_set, threshold);
id_set_short_removed= idSetRemove(id_set, id_too_short);   
id_set_cv= idSetRemove(id_set_short_removed, id_set_holdout);


rng(3, 'twister');  
fold = 10;
index_cv = crossvalind('Kfold', length(id_set_cv), 10);
range_k = 12-1; 
llh_test = zeros(fold, range_k);

%+++++++++++++++
% repeat 10 times of 10-fold cv to obtain mean llh of test set
for i=1:1:10;
    fprintf('****** 10 fold cv (preprocessing): test fold = %d ****** \n', i);
    test = (index_cv == i); train = ~ test;  
    id_set_train = id_set_cv(train);               
    id_set_test = id_set_cv(test);

    [float_train, float_test]= ...
        floatsPreprocessorTrainTest(float_table, ...
        id_set_train, id_set_test);

    N = size(id_set_train,1);     
    size_col= 12;   
    d = size_col;     
    for k = 1 : 1: (size_col-1) 
        fprintf('******** 10 fold cv (fitting and testing): test_fold = %d,  k = %d ******** \n', i, k);
        model = onefloat_setModel(d,k,N);  
        % model fitting 
        [float_train, model, llh_train] = floatLDSFitting(float_train, model);
        % log-likelihood
        [float_test, llh_test(i, k)] = floatLDSTestingllh(float_test, model);
    end
end

fprintf('pause\n');

