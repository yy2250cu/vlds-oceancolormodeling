function [float_test, llh_test] = floatLDSTestingllh(float_test, model)
% function for generating LDS fitting information for float_test
% input:
% float_test -- obersvation data input
% model -- LDS model parameter
% output:
% float_test -- with more information on mean and v
%                   ariance and llh
%
% * one Expectation step
% written by Yan Yan (yy2250@columbia.edu)

N = size(float_test, 2);
llh_test = 0;
for i = 1:N
     [float_test(i).nu, float_test(i).U, float_test(i).Ezz, float_test(i).Ezy,...
         float_test(i).llh] = kalmanSmoother(float_test(i).X, model);
     llh_test = llh_test + float_test(i).llh;
end

end