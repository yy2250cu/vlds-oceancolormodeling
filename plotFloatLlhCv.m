function plotFloatLlhCv(float, llh, figNo)
% generate the predictive and llh plot for the cv dataset
% input:
% float -- structure containt a particular float
% llh -- llh history
% figNo -- figure number to use
% output:
% generate the llh convergence figures for this particular float
% written by Yan Yan (yy2250@columbia.edu)

id1 = 27139.03;
id2 = 43744.06; 
id3 = 34721.03;
id4 = 43746.06;
id5= 53363.03;

N = size(float,2);

for i = 1:N   
    if float(i).id == id1    index1 = i; end
    if float(i).id == id2    index2 = i; end
    if float(i).id == id3	 index3 = i; end
    if float(i).id == id4	 index4 = i; end
    if float(i).id == id5	 index5 = i; end
end

figure(figNo)

llhLength = length(llh);
llhX = 1:1:llhLength ; % all floats
fllhX = llhX +1;   % individual float
semilogy(llhX, llh(llhX), 'b--', 'LineWidth',1.5)
hold on;
semilogy(llhX, float(index1).llh(fllhX), 'r', 'LineWidth',1.5);
semilogy(llhX, float(index2).llh(fllhX), 'k', 'LineWidth',1.5);
semilogy(llhX, float(index3).llh(fllhX), 'g', 'LineWidth',1.5);
semilogy(llhX, float(index4).llh(fllhX), 'm', 'LineWidth',1.5);
semilogy(llhX, float(index5).llh(fllhX), 'c', 'LineWidth',1.5);
xlabel('iteration', 'fontsize',14);
ylabel('log-likelihood', 'fontsize',14)
legend_obj = legend('llh of complete data', ['llh of id ' num2str(id1)], ... 
                    ['llh of id ' num2str(id2)], ['llh of id ' num2str(id3)], ...
                     ['llh of id ' num2str(id4)],   ['llh of id ' num2str(id5)],...
                     'Location', 'East');
legend_obj.FontSize = 14
title({'log-likelihood at each iteration on the cross-validation dataset',...
    'using 10-fold cross-validated latent dimension k=11'}, 'fontsize',14)
hold off;

end