function model = onefloat_setModel(d,k,N)
% Generate a data sequence from linear dynamic system.
% Input:
%   d: dimension of data
%   k: dimension of latent variable
%   N: count of floats in this dataset
% Output:
%   model: model structure
% written by Yan Yan (yy2250@columbia.edu)

rng(187);

A = randn(k,k);
G = iwishrnd(eye(k),k);
C = randn(d,k);
S = iwishrnd(eye(d),d);
mu0 = randn(k,1);
P0 = iwishrnd(eye(k),k);


model.N = N; 
model.A = A;  
model.G = G; 
model.C = C; 
model.S = S;  
model.mu0 = mu0; 
model.P0 = P0;  

model.d = d; 
model.k = k; 
end