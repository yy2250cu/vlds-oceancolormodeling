function model = floatMaximization(float, model)
% M-step to generate new model parameters for LDS
% input:
% float -- the structure that contain all the floats in training set
% model -- the old model parameters (contains number of floats)
% output:
% model -- new model parameters
% written by Yan Yan (yy2250@columbia.edu)

d = model.d;   
k = model.k;   
N = model.N; 

mu0 = zeros(k, 1);  
P0 = zeros(k,k);      
Atop = zeros(k,k);
Abottom = zeros(k,k);
Ctop = zeros(d,k);
Cbottom = zeros(k,k);
Gtop = zeros(k,k);
Gbottom = 0;
S_top = zeros(d,d);
Sbottom = 0;

% first loop over all floats to update the 
% transformation operator A, C, mu0, P0,
for i = 1: N
     mu0 = mu0 + float(i).nu(:,1);
     P0 = P0 + float(i).U(:,:,1);
     
     % Ezzn, Ezz1, Ezz2, Ezy
     Tn = float(i).Tn;
     Ezzn = sum(float(i).Ezz, 3);
     Ezz1 = Ezzn - float(i).Ezz(:,:,Tn);
     Ezz2 = Ezzn - float(i).Ezz(:,:,1);
     Ezy = sum(float(i).Ezy, 3);
     
     Atop = Atop + Ezy ;                 % prepare for Anew
     Abottom = Abottom + Ezz1 ;
     
     Xnu =  float(i).X* (float(i).nu)';    % prepare for Cnew
     Ctop = Ctop + Xnu;                  % prepare for Ctop
     Cbottom = Cbottom + Ezzn;

end

mu0 = mu0/N;  % average across all floats
P0 = P0/N;
Anew = Atop/Abottom;
Cnew = Ctop/Cbottom;


% second loop over all floats to update the
% Variance Computation G, S
for i = 1:N
    
    % repeat it again -- Ezzn, Ezz1, Ezz2, Ezy
    Tn = float(i).Tn;
    Ezzn = sum(float(i).Ezz, 3);
    Ezz1 = Ezzn - float(i).Ezz(:,:,Tn);
    Ezz2 = Ezzn - float(i).Ezz(:,:,1);
    Ezy = sum(float(i).Ezy, 3);
    
    EzyA = Ezy* Anew';
    Gtop = Gtop + Ezz2 - (EzyA + EzyA') + Anew* Ezz1*Anew';
    Gbottom = Gbottom + Tn - 1;   % Tn is variying!
    
    Xnu =  float(i).X* (float(i).nu)'; 
    XnuC = Xnu*Cnew';
    S_top = S_top + float(i).X*(float(i).X)'-(XnuC+XnuC')+Cnew*Ezzn*Cnew';
    Sbottom = Sbottom + Tn;
end

Gnew = Gtop/Gbottom;
Snew = S_top/Sbottom;

model.A = Anew;
model.G = Gnew;
model.C = Cnew;
model.S = Snew;
model.mu0 = mu0;
model.P0 = model.P0;



end
