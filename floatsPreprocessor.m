function [X, Tn]  = floatsPreprocessor( float_table, id )
%floatsPreprocessor Given datatable and id, prepare the data
%   input: 
%   float_table -- data source in matlab datatable
%   id -- the id number of a drifter, it is type float
%   output:
%   X -- the prepared the observation of float i
%   Tn -- the length of the time serie of the float dataset 
% written by Yan Yan (yy2250@columbia.edu)

fprintf('preprocessing float id %f\n', id);
mask =  (float_table.id == id);
float = float_table(mask,:);

%%% feature sensative step
float(:,{'Var1', 'id'} ) = []; 
float.Properties.RowNames = float.time; 
float(:,{'time'}) = []; 
float = float(:, [1 2 3 4 5 7 8 9 10 11 12 6]);  % reorder rows   -- based on the LDF framework
float = sortrows(float,'RowNames');    % sorted ascending    
float_array = float{:,:} ;                      

tmp = sum(isnan(float{:,:}) );
countNans = sum(tmp);
if isequal(countNans, 0)   fprintf('good on processing \n'); 
else error('error on processing \n'); 
end

X = float_array';   
Tn = size(X, 2);    
end

