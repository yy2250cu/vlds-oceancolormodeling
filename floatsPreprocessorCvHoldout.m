function [float_cv, float_holdout]= floatsPreprocessorCvHoldout(float_table,...
    id_set_cv, id_set_holdout)
% function for generating CV {both training & testing}
% and hold-out dataset
% input:
% float_table -- data for all float 
% id_set_cv-- cv set id
% id_set_holdout --  holdout set id
% output:
%  float_train -- training set data
%  float_test -- testing set data
% written by Yan Yan (yy2250@columbia.edu)

id_count_cv = length(id_set_cv);
for i = 1:  id_count_cv
    fprintf('training dataset %d \n', i);
    float_cv(i).id = id_set_cv(i);
    [float_cv(i).X, float_cv(i).Tn] = floatsPreprocessor(float_table, id_set_cv(i));
end

id_count_holdout = length(id_set_holdout);
for i = 1:  id_count_holdout
    fprintf('testing dataset %d \n', i);
    float_holdout(i).id = id_set_holdout(i);
    [float_holdout(i).X, float_holdout(i).Tn] = floatsPreprocessor(float_table, id_set_holdout(i));
end

end